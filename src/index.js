const express = require('express');
const SocketIO = require('socket.io');
// const mongoose = require('mongoose')
const app = express();
const morgan = require('morgan');


//settings
const port = process.env.PORT || 3000;
app.set('port', port);
app.set('json spaces', 2);

//static files
app.use(express.static('public'));

//middleware
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

//starting the server
const server = app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});


// websockets
let io = SocketIO(server);

io.on('connection', (socket) => {
    socket.emit('bodyPedidos');
    socket.on('bodyPedidos', (data) => {
        io.sockets.emit('bodyPedidos', data);
    });
});


app.post('/api/pedidos/', (req, res) => {
    let ioClient = require('socket.io-client');
    let Client = ioClient.connect('http://localhost:' + 3000, { 'forceNew': true });
    let request = req.body;

    Client.on('bodyPedidos', () => {
        io().emit('bodyPedidos', request);
    });
    Client.emit('bodyPedidos', request);
    console.log(request);
    res.json(request);
});



// // moongose connection

// mongoose.connect(process.env.MONGODB_URI)
//     .then(() => console.log('conectado a la base de datos'))
//     .catch((error) => console.log(error));

